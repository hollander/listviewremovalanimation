package android.view;

import java.util.HashMap;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

public class ViewPropertyAnimatorCompat {
	
	private static final HashMap<Animator, Runnable> mEndActionMap = new HashMap<Animator, Runnable>();

	private static class AnimationListener extends AnimatorListenerAdapter {
		private Runnable mEndAction;

		AnimationListener(Runnable endAction) {
			mEndAction = endAction;
		}

		@Override
		public void onAnimationCancel(Animator animation) {
			mEndAction = null;
		}

		@Override
		public void onAnimationEnd(Animator animation) {
			if(mEndAction != null) {
				mEndAction.run();
			}
			mEndAction = null;
		}
	}

	public static ViewPropertyAnimator withEndAction(ViewPropertyAnimator animator, Runnable endAction) {
		return animator.setListener(new AnimationListener(endAction));
	}
}
